﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;

namespace ControlEscolar2
{
    public partial class Alumnos : Form
    {
        private AlumnoManejador _alumnoManejador;
        private EstadoManejador _estadoManejador;
        private MunicipiosManejador _municipiomanejador;
        public Alumnos()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _estadoManejador = new EstadoManejador();
            _municipiomanejador = new MunicipiosManejador();

            dtp1.Format = DateTimePickerFormat.Custom;
            dtp1.CustomFormat = "yyyy-MM-dd";
        }

        private void Alumnos_Load(object sender, EventArgs e)
        {

            BuscarAlumnos("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            lblCod.Visible = false;
            lblNumero.Visible = false;
   
            MostrarEstado("");         
        }
        private void MostrarEstado(string filtro)
        {
            cmbEstado.DataSource = _estadoManejador.GetEstados(filtro);
            cmbEstado.DisplayMember = "estado";
            cmbEstado.ValueMember = "codigoestado";
        }
        private void MostrarMunic(string filtro)
        {
            cmbMunic.DataSource = _municipiomanejador.GetMunicipios(cmbEstado.SelectedValue.ToString());
            //cmbMunic.ValueMember = "idmunicipio";
            cmbMunic.DisplayMember = "municipio";         
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtApellidop.Enabled = activar;
            txtApellidom.Enabled = activar;
            dtp1.Enabled = activar;
            txtDomicilio.Enabled = activar;
            txtEmail.Enabled = activar;
            cmbSexo.Enabled = activar;
            cmbEstado.Enabled = activar;
            cmbMunic.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtApellidop.Text = "";
            txtApellidom.Text = "";
            dtp1.Text = "";
            txtDomicilio.Text = "";
            txtEmail.Text = "";
            cmbSexo.Text = "";
            lblNumero.Text = "0";
            cmbMunic.Text = "";
            cmbEstado.Text = "";           
        }
        private void BuscarAlumnos(string filtro)
        {
            dgvAlumnos.DataSource = _alumnoManejador.GetAlumnos(filtro);
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtNombre.Focus();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarAlumno();
                LimpiarCuadros();
                BuscarAlumnos("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void GuardarAlumno()
        {
            _alumnoManejador.Guardar(new Alumno
            {
                Numerocontrol = Convert.ToInt32(lblNumero.Text),
                Nombre = txtNombre.Text,
                Apellidopaterno = txtApellidop.Text,
                Apellidomaterno = txtApellidom.Text,
                
                Fechanacimiento = dtp1.Text,
                Domicilio = txtDomicilio.Text,
                Email = txtEmail.Text,
                Sexo = cmbSexo.Text,
                Estado = cmbEstado.Text,
                Municipio = cmbMunic.Text
            });
        }
        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarAlumnos(txtBuscar.Text);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estás seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarAlumno();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void EliminarAlumno()
        {
            var NumeroControl = dgvAlumnos.CurrentRow.Cells["numerocontrol"].Value;
            _alumnoManejador.Eliminar(Convert.ToInt32(NumeroControl));
        }

        private void DgvAlumnos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarAlumno();
                BuscarAlumnos("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void ModificarAlumno()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblCod.Visible = true;
            lblNumero.Visible = true;

            lblNumero.Text = dgvAlumnos.CurrentRow.Cells["Numerocontrol"].Value.ToString();
            txtNombre.Text = dgvAlumnos.CurrentRow.Cells["Nombre"].Value.ToString();
            txtApellidop.Text = dgvAlumnos.CurrentRow.Cells["Apellidopaterno"].Value.ToString();
            txtApellidom.Text = dgvAlumnos.CurrentRow.Cells["Apellidomaterno"].Value.ToString();
            dtp1.Text = dgvAlumnos.CurrentRow.Cells["Fechanacimiento"].Value.ToString();
            txtDomicilio.Text = dgvAlumnos.CurrentRow.Cells["Domicilio"].Value.ToString();
            txtEmail.Text = dgvAlumnos.CurrentRow.Cells["Email"].Value.ToString();
            cmbSexo.Text = dgvAlumnos.CurrentRow.Cells["Sexo"].Value.ToString();
            cmbEstado.Text = dgvAlumnos.CurrentRow.Cells["estado"].Value.ToString();
            cmbMunic.Text = dgvAlumnos.CurrentRow.Cells["municipio"].Value.ToString();          
        }

        private void CmbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            MostrarMunic("");
        }

        private void CmbMunic_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void CmbEstado_Click(object sender, EventArgs e)
        {

        }

        private void Label11_Click(object sender, EventArgs e)
        {

        }
    }
}
