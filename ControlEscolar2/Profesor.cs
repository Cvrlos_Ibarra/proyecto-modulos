﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;

namespace ControlEscolar2
{
    public partial class Profesor : Form
    {
        private ProfesorManejador _profesorManejador;
        private EstadoManejador _estadoManejador;
        private MunicipiosManejador _municipiomanejador;

       


        public Profesor()
        {
            InitializeComponent();
           

            _profesorManejador = new ProfesorManejador();
            _estadoManejador = new EstadoManejador();
            _municipiomanejador = new MunicipiosManejador();

            dtp1.Format = DateTimePickerFormat.Custom;
            dtp1.CustomFormat = "yyyy-MM-dd";
            dtpIngreso.Format = DateTimePickerFormat.Custom;
            dtpIngreso.CustomFormat = "yyyy-MM-dd";
        }

        private void Profesor_Load(object sender, EventArgs e)
        {

            txtN.Visible = false;
            BuscarProfesores("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();

            lblx.Text = "0";
            lblx.Visible = false;
            nocontrol.Visible = false;
            

            MostrarEstado("");
            //MostrarIds("2018");
            txtN.Enabled = false;

            
            
        }

        


        private void MostrarEstado(string filtro)
        {
            cmbEstado.DataSource = _estadoManejador.GetEstados(filtro);
            cmbEstado.DisplayMember = "estado";
            cmbEstado.ValueMember = "codigoestado";
            
        }
        private void MostrarMunic(string filtro)
        {
            cmbMunic.DataSource = _municipiomanejador.GetMunicipios(cmbEstado.SelectedValue.ToString());
            //cmbMunic.ValueMember = "idmunicipio";
            cmbMunic.DisplayMember = "municipio";
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtApellidop.Enabled = activar;
            txtApellidom.Enabled = activar;
            dtp1.Enabled = activar;
            txtDireccion.Enabled = activar;
            txtCedula.Enabled = activar;
            txtTitulo.Enabled = activar;
            cmbEstado.Enabled = activar;
            cmbMunic.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtN.Text = "";
            txtNombre.Text = "";
            txtApellidop.Text = "";
            txtApellidom.Text = "";
            dtp1.Text = "";
            txtDireccion.Text = "";
            txtCedula.Text = "";
            txtTitulo.Text = "";
            
            cmbMunic.Text = "";
            cmbEstado.Text = "";
        }



        private void BuscarProfesores(string filtro)
        {
            dgvProfe.DataSource = _profesorManejador.GetProfesores(filtro);
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            MaxId();
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtNombre.Focus();
            txtN.Visible = true;
            nocontrol.Visible = true;
            
        }
        public void MaxId()
        {
            try
            {
                string anio = dtpIngreso.Value.ToString("yyyy");
                string max = (from DataGridViewRow row in dgvProfe.Rows
                                  //where row.Cells[0].FormattedValue.ToString() != string.Empty
                              where row.Cells[0].FormattedValue.ToString().Contains(anio)
                              //select (row.Cells[0].FormattedValue)).Max().ToString().Remove(0,5);
                              select (row.Cells[0].FormattedValue)).Max().ToString().Substring(6);

                int maxi = int.Parse(max) + 1;

                if (int.Parse(max) < 10)
                {
                    txtN.Text = "D" + dtpIngreso.Value.ToString("yyyy") + "0" + maxi.ToString();
                }
                else
                {
                    txtN.Text = "D" + dtpIngreso.Value.ToString("yyyy") + (int.Parse(max) + 1).ToString();
                }
            }
            catch (Exception)
            {

                txtN.Text = "D" + dtpIngreso.Value.ToString("yyyy") + "01";
            }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            nocontrol.Visible = false;
            txtN.Visible = false;
            
            try
            {
                GuardarProfe();
                LimpiarCuadros();
                BuscarProfesores("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GuardarProfe()
        {
            _profesorManejador.Guardar(new Entidades.ControlEscolar2.Profesor
            {
                Numerocontrol = txtN.Text,
                Nombre = txtNombre.Text,
                App = txtApellidop.Text,
                Apm = txtApellidom.Text,
                Direccion = txtDireccion.Text,
                Ciudad = cmbMunic.Text,
                Estado = cmbEstado.Text,
                Cedula = Convert.ToInt32(txtCedula.Text),
                Titulo = txtTitulo.Text,
                Fechan = dtp1.Text
                
            },lblx.Text);
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
            nocontrol.Visible = false;
            txtN.Visible = false;
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarProfesores(txtBuscar.Text);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estás seguro que deseas eliminar este registro", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarProfe();
                    BuscarProfesores("");

                    


                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void EliminarProfe()
        {
            var NumeroControl = dgvProfe.CurrentRow.Cells["numerocontrol"].Value;
            _profesorManejador.Eliminar(NumeroControl.ToString());
        }



        








            private void DgvProfe_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            txtN.Visible = true;
            try
            {
                lblx.Text = "1";
                ModificarProfe();
                BuscarProfesores("");

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        private void ModificarProfe()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);
            lblx.Text = "1";
            

            txtN.Text = dgvProfe.CurrentRow.Cells["Numerocontrol"].Value.ToString();
            txtNombre.Text = dgvProfe.CurrentRow.Cells["Nombre"].Value.ToString();
            txtApellidop.Text = dgvProfe.CurrentRow.Cells["App"].Value.ToString();
            txtApellidom.Text = dgvProfe.CurrentRow.Cells["Apm"].Value.ToString();
            txtDireccion.Text = dgvProfe.CurrentRow.Cells["direccion"].Value.ToString();
            cmbMunic.Text = dgvProfe.CurrentRow.Cells["ciudad"].Value.ToString();
            cmbEstado.Text = dgvProfe.CurrentRow.Cells["estado"].Value.ToString();
            txtCedula.Text = dgvProfe.CurrentRow.Cells["cedula"].Value.ToString();
            txtTitulo.Text = dgvProfe.CurrentRow.Cells["titulo"].Value.ToString();
            dtp1.Text = dgvProfe.CurrentRow.Cells["Fechan"].Value.ToString();
           
            
        }

        private void CmbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            MostrarMunic("");
        }

        private void CmbId_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            
        }

        private void DtpIngreso_ValueChanged(object sender, EventArgs e)
        {
            MaxId();
        }

        private void BtnEstudios_Click(object sender, EventArgs e)
        {
            Estudio estudio = new Estudio();
            estudio.ShowDialog();
           
        }
    }
}
