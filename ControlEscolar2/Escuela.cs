﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;
using AccesoaDatos.ControlEscolar2;
using System.IO;

namespace ControlEscolar2
{
    public partial class Escuela : Form
    {
        private OpenFileDialog _imagenjpg;

        //private OpenFileDialog _archivopdf;

        private string _ruta;

        string img;
        string img2;


        private EscuelaManejador _escuelamanejador;
        public Escuela()
        {
            
            InitializeComponent();
            _imagenjpg = new OpenFileDialog();
            //_archivopdf = new OpenFileDialog();
            _ruta = Application.StartupPath + "\\Logo\\";

            _escuelamanejador = new EscuelaManejador();
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {

        }

        private void Escuela_Load(object sender, EventArgs e)
        {
            try
            {
                


                cuadritos();
                BuscarEscuela("");
                //LimpiarCuadros();
                //lblId.Text = "0";
                //btnGuardar.Enabled = false;

                //try
                //{
                ModificarEscuela();
                //editar2();
                //pblogo.ImageLocation = _ruta + img;
                //}
                //catch (Exception)
                //{

                //  MessageBox.Show("Actualmente sin registros");
                //}
                
                pblogo.Image = Image.FromFile(_ruta + img);

                //img = dgvEscuela.CurrentRow.Cells["logo"].Value.ToString();

            }
            catch (Exception)
            {

                MessageBox.Show("No hay registro");
            }
            
        }

        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtDomicilio.Text = "";
            txtRfc.Text = "";
            txtTelefono.Text = "";
            lblId.Text = "";
            txtPaginaweb.Text = "";
            txtEmail.Text = "";
            txtPaginaweb.Text = "";
            txtDirector.Text = "";

        }
        private void BuscarEscuela(string filtro)
        {
            //dgvEstudios.DataSource = _estudiomanejador.GetEstudios(filtro);
           

        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            //try
            //{
                GuardarEscuela();
                GuardarImagenjpg();
                BuscarEscuela("");
                btnGuardar.Enabled = false;
           // }
           // catch
           // {
                //MessageBox.Show("El logotipo ya Existe");
           // }
            
        }
        private void GuardarEscuela()
        {
            _escuelamanejador.Guardar(new Entidades.ControlEscolar2.Escuela
            {
                Id = int.Parse(lblId.Text),
                Nombre = txtNombre.Text,
                Rfc = txtRfc.Text,
                Domicilio = txtDomicilio.Text,
                Telefono = int.Parse(txtTelefono.Text),
                Email = txtEmail.Text,
                Pagina = txtPaginaweb.Text,
                Director = txtDirector.Text,
                Logo = img

            });
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            LimpiarCuadros();
            pblogo.ImageLocation = _ruta + img;
            //pblogo.Refresh();
            //pblogo.Image = null;
            //img = "";
            //pblogo.Image = Image.FromFile(_ruta + img);

        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            EliminarEscuela();
            EliminarImagenjpg();
            
            BuscarEscuela("");
            btnGuardar.Enabled = true;
        }
        private void EliminarEscuela()
        {
            
            //var id = dgvEscuela.CurrentRow.Cells["id"].Value;

            var id = lblId.Text;
            _escuelamanejador.Eliminar(Convert.ToInt32(id));
            EliminarImagenjpg();
        }

        private void EliminarArchivo2()
        {
            var obtenerArchivos = Directory.GetFiles(_ruta, "*.png");
            FileInfo archivoAnterior;

            if (obtenerArchivos.Length != 0)
            {
                //Código para reemplazar imagen
                archivoAnterior = new FileInfo(obtenerArchivos[0]);
                if (archivoAnterior.Exists)
                {
                    archivoAnterior.Delete();
                    MessageBox.Show("Imagen eliminada");

                }

            }

        }

        private void DgvEscuela_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void cuadritos()
        {
            var t = new DataSet();
            t = _escuelamanejador.consulta();

            if (t.Tables[0].Rows.Count== 0)
            {
                btnGuardar.Enabled = true;
                lblId.Text = "0";
            }
            else
            {
                btnGuardar.Enabled = false;
            }
        }

        public void editar()
        {
            txtNombre.Enabled = true;
            txtDomicilio.Enabled = true;
            txtRfc.Enabled = true;
            txtTelefono.Enabled = true;
            txtEmail.Enabled = true;
            txtPaginaweb.Enabled = true;
            txtDirector.Enabled = true;
        }
        public void editar2()
        {
            txtNombre.Enabled = false;
            txtDomicilio.Enabled = false;
            txtRfc.Enabled = false;
            txtTelefono.Enabled = false;
            txtEmail.Enabled = false;
            txtPaginaweb.Enabled = false;
            txtDirector.Enabled = false;

            
        }

        private void ModificarEscuela()
        {
            var t = new DataSet();
            t = _escuelamanejador.consulta();




            lblId.Text = t.Tables[0].Rows[0]["id"].ToString();


            txtNombre.Text = t.Tables[0].Rows[0]["nombre"].ToString();
            txtDomicilio.Text = t.Tables[0].Rows[0]["domicilio"].ToString();
            txtRfc.Text = t.Tables[0].Rows[0]["rfc"].ToString();
            txtTelefono.Text = t.Tables[0].Rows[0]["telefono"].ToString();
            txtEmail.Text = t.Tables[0].Rows[0]["email"].ToString();
            txtPaginaweb.Text = t.Tables[0].Rows[0]["pagina"].ToString();
            txtDirector.Text = t.Tables[0].Rows[0]["director"].ToString();
            img = t.Tables[0].Rows[0]["logo"].ToString();



            //lblId.Visible = true;


            /*lblId.Text = dgvEscuela.CurrentRow.Cells["id"].Value.ToString();
            txtNombre.Text = dgvEscuela.CurrentRow.Cells["nombre"].Value.ToString();
            txtDomicilio.Text = dgvEscuela.CurrentRow.Cells["domicilio"].Value.ToString();
            txtRfc.Text = dgvEscuela.CurrentRow.Cells["rfc"].Value.ToString();
            txtTelefono.Text = dgvEscuela.CurrentRow.Cells["telefono"].Value.ToString();
            txtEmail.Text = dgvEscuela.CurrentRow.Cells["email"].Value.ToString();
            txtPaginaweb.Text = dgvEscuela.CurrentRow.Cells["pagina"].Value.ToString();
            txtDirector.Text = dgvEscuela.CurrentRow.Cells["director"].Value.ToString();
            img = dgvEscuela.CurrentRow.Cells["logo"].Value.ToString();*/


            




            /*if (t.Tables[0].Rows.Count == 0)
            {
                pblogo.Image = null;
            }
            else
            {
                pblogo.Image = Image.FromFile(_ruta + img);
            }*/






        }

        private void BtnCargar_Click(object sender, EventArgs e)
        {
            CargarImagenjpg();
            //pblogo.ImageLocation = _ruta + img;
        }
        private void CargarImagenjpg()
        {

            _imagenjpg.Filter = "Imagen tipo (*.jpg)|*.jpg| imagen tipo (*.png)|*.png";
            _imagenjpg.Title = "Cargar Imagen";


            _imagenjpg.ShowDialog();

            if (_imagenjpg.FileName != "")
            {
                var archivo = new FileInfo(_imagenjpg.FileName);

                if (archivo.Length <= 200000)
                {
                    img = archivo.Name;
                    pblogo.Image = Image.FromFile(archivo.FullName);
                    //pblogo.Image = Image.FromFile(_ruta + img);
                    //pblogo.ImageLocation = _ruta + img;

                }
                else
                {
                    MessageBox.Show("El archivo no puede pesar más de 5mb");
                }             
            }
        }


        private void GuardarImagenjpg()
        {

            if (_imagenjpg.FileName.Contains(".jpg"))
            {
                if (_imagenjpg.FileName != null)
                {
                    if (_imagenjpg.FileName != "")
                    {
                        var archivo = new FileInfo(_imagenjpg.FileName);

                        if (Directory.Exists(_ruta))
                        {
                            //Codigo para agregar archivo

                            var obtenerArchivos = Directory.GetFiles(_ruta, "*.jpg");
                            FileInfo archivoAnterior;
                            MessageBox.Show("Registro Guardado");

                            if (obtenerArchivos.Length != 0)
                            {
                                //Código para reemplazar imagen
                                archivoAnterior = new FileInfo(obtenerArchivos[0]);
                                if (archivoAnterior.Exists)
                                {
                                    archivoAnterior.Delete();
                                    
                                    archivo.CopyTo(_ruta + archivo.Name);
                                    //img = archivo.Name;
                                    //MessageBox.Show("Archivo Reemplazado");
                                }

                            }
                            else
                            {
                                archivo.CopyTo(_ruta + archivo.Name);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(_ruta);
                            archivo.CopyTo(_ruta + archivo.Name);
                        }

                    }

                }
            }
            if (_imagenjpg.FileName.Contains(".png"))
            {
                if (_imagenjpg.FileName != null)
                {
                    if (_imagenjpg.FileName != "")
                    {
                        var archivo = new FileInfo(_imagenjpg.FileName);

                        if (Directory.Exists(_ruta))
                        {
                            //Codigo para agregar archivo

                            var obtenerArchivos = Directory.GetFiles(_ruta, "*.png");
                            FileInfo archivoAnterior;
                            

                            if (obtenerArchivos.Length != 0)
                            {
                               // try
                               // {
                                    
                                    //Código para reemplazar imagen
                                    archivoAnterior = new FileInfo(obtenerArchivos[0]);
                                    if (archivoAnterior.Exists)
                                    {
                                        archivoAnterior.Delete();
                                        archivo.CopyTo(_ruta + archivo.Name);
                                        //MessageBox.Show("Archivo Reemplazado");
                                        MessageBox.Show("Registro Guardado");
                                    }
                               // }
                                //catch
                                //{
                                  //  MessageBox.Show("El logotipo ya existe");
                               // }
                                


                            }
                            else
                            {
                                archivo.CopyTo(_ruta + archivo.Name);
                            }


                        }
                        else
                        {
                            Directory.CreateDirectory(_ruta);
                            archivo.CopyTo(_ruta + archivo.Name);
                        }
                    }
                }
            }

            

        }
        private void EliminarImagenjpg()
        {
            if (_imagenjpg.FileName.Contains(".jpg"))
            {
                if (MessageBox.Show("Estas seguro de eliminar la imagen", "Eliminar Imagen", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (Directory.Exists(_ruta))
                    {
                        //Codigo para agregar archivo

                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.jpg");
                        FileInfo archivoAnterior;

                        if (obtenerArchivos.Length != 0)
                        {
                            //Código para reemplazar imagen
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();
                                MessageBox.Show("Imagen eliminada");

                            }

                        }

                    }
                }
            }


            if (_imagenjpg.FileName.Contains(".png"))
            {
                if (MessageBox.Show("¿Estás seguro de eliminar el archivo?", "Eliminar Archivo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    if (Directory.Exists(_ruta))
                    {
                        //Codigo para agregar archivo

                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.png");
                        FileInfo archivoAnterior;

                        if (obtenerArchivos.Length != 0)
                        {
                            //Código para reemplazar imagen
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();
                                MessageBox.Show("Archivo eliminado");

                            }

                        }

                    }
                }
            }


           



        }

        private void BtnBorrar_Click(object sender, EventArgs e)
        {
            img = "";
            pblogo.Image = null;
        }

        private void Datos_Click(object sender, EventArgs e)
        {

        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            ModificarEscuela();
            editar();
            btnGuardar.Enabled = true;
            pblogo.ImageLocation = _ruta + img;
            //pblogo.Image = Image.FromFile(_ruta + img);

        }
    }
}
