﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlEscolar2
{
    public partial class PantallaPrincipal : Form
    {
        public PantallaPrincipal()
        {
            InitializeComponent();
        }

        private void MenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            
        }

        private void MsMenuPrincipal_Click(object sender, EventArgs e)
        {

        }

        private void UsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUsuarios usuarios = new frmUsuarios();
            usuarios.ShowDialog();
        }

        private void MsAlumnos_Click(object sender, EventArgs e)
        {
            Alumnos alumnos = new Alumnos();
            alumnos.ShowDialog();
        }

        private void ProfesoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Profesor profesor = new Profesor();
            profesor.ShowDialog();
        }

        private void PantallaPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void MateriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Materias materias = new Materias();
            materias.ShowDialog();
        }

        private void EscuelaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Escuela escuela = new Escuela();
            escuela.ShowDialog();
        }
    }
}
