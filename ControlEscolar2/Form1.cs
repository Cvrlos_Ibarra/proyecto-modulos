﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades.ControlEscolar2;
using LogicaNegocios.ControlEscolar2;

namespace ControlEscolar2
{
    public partial class frmUsuarios : Form
    {
        private UsuarioManejador _usuarioManejador;
        private Usuario _usuario;
        public frmUsuarios()
        {
            InitializeComponent();
            _usuarioManejador = new UsuarioManejador();
            _usuario = new Usuario();
        }

        private void FrmUsuarios_Load(object sender, EventArgs e)
        {
            BuscarUsuarios("");
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }
        private void ControlarBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool activar)
        {
            txtNombre.Enabled = activar;
            txtApellidop.Enabled = activar;
            txtApellidom.Enabled = activar;
            txtContrasenia.Enabled = activar;
        }
        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtApellidop.Text = "";
            txtApellidom.Text = "";
            txtContrasenia.Text = "";
            lblId.Text = "0";
        }
        private void BuscarUsuarios(string filtro)
        {
            dgvUsuarios.DataSource = _usuarioManejador.GetUsuarios(filtro);
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlarBotones(false, true, true, false);
            ControlarCuadros(true);
            txtNombre.Focus();
        }

        private bool ValidarUsuario()
        {
            var tupla = _usuarioManejador.ValidarUsuario(_usuario);
            var valido = tupla.Item1;
            var mensaje = tupla.Item2;

            if (!valido)
            {
                MessageBox.Show(mensaje, "Error de validación", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return valido;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {

            
            try
            {
                CargarUsuario();
                if (ValidarUsuario())
                {
                    GuardarUsuario();
                    LimpiarCuadros();
                    BuscarUsuarios("");
                    ControlarBotones(true, false, false, true);
                    ControlarCuadros(false); 
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

                
            }



        }
        private void CargarUsuario()
        {
            _usuario.Idusuario = Convert.ToInt32(lblId.Text);
            _usuario.Nombre = txtNombre.Text;
            _usuario.App = txtApellidop.Text;
            _usuario.Apm = txtApellidom.Text;
            _usuario.Contrasenia = txtContrasenia.Text;
        }

        private void GuardarUsuario()
        {
            _usuarioManejador.Guardar(_usuario);
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlarBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarUsuarios(txtBuscar.Text);
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estás seguro que deseas eliminar este registro","Eliminar Registro",MessageBoxButtons.YesNo)==DialogResult.Yes)
            {
            try
            {
                EliminarUsuario();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

            }
        }

        private void EliminarUsuario()
        {
            var IdUsuario = dgvUsuarios.CurrentRow.Cells["Idusuario"].Value;
            _usuarioManejador.Eliminar(Convert.ToInt32(IdUsuario));
        }

        private void DgvUsuarios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarUsuario();
                BuscarUsuarios("");

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }
        private void ModificarUsuario()
        {
            ControlarCuadros(true);
            ControlarBotones(false, true, true, false);

            lblId.Text= dgvUsuarios.CurrentRow.Cells["Idusuario"].Value.ToString();
            txtNombre.Text = dgvUsuarios.CurrentRow.Cells["Nombre"].Value.ToString();
            txtApellidop.Text = dgvUsuarios.CurrentRow.Cells["App"].Value.ToString();
            txtApellidom.Text = dgvUsuarios.CurrentRow.Cells["Apm"].Value.ToString();
            txtContrasenia.Text = dgvUsuarios.CurrentRow.Cells["Contrasenia"].Value.ToString();
        }
    }
}
