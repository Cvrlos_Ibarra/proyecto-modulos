﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Materias
    {
        private string _idmateria;
        private string _nombremateria;
        private int _teoria;
        private int _practica;
        private string _semestre;
        private string _materiaAnterior;
        private string _materiaSiguiente;
        private string _carrera;

        public string Idmateria { get => _idmateria; set => _idmateria = value; }
        public string Nombremateria { get => _nombremateria; set => _nombremateria = value; }
        public int Teoria { get => _teoria; set => _teoria = value; }
        public int Practica { get => _practica; set => _practica = value; }
        public string Semestre { get => _semestre; set => _semestre = value; }
        public string MateriaAnterior { get => _materiaAnterior; set => _materiaAnterior = value; }
        public string MateriaSiguiente { get => _materiaSiguiente; set => _materiaSiguiente = value; }
        public string Carrera { get => _carrera; set => _carrera = value; }
    }
}
