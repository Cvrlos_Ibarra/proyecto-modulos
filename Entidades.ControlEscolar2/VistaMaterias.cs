﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class VistaMaterias
    {
        private string _idmateria;
        private string _nombremateria;
        private int _horasteoria;
        private int _horaspractica;
        private int _creditos;
        private string semestre;
        private string _materiaAnterior;
        private string _materiaPosterior;
        private string _carrera;

        public string Idmateria { get => _idmateria; set => _idmateria = value; }
        public string Nombremateria { get => _nombremateria; set => _nombremateria = value; }
        public int Horasteoria { get => _horasteoria; set => _horasteoria = value; }
        public int Horaspractica { get => _horaspractica; set => _horaspractica = value; }
        public int Creditos { get => _creditos; set => _creditos = value; }
        public string Semestre { get => semestre; set => semestre = value; }
        public string MateriaAnterior { get => _materiaAnterior; set => _materiaAnterior = value; }
        public string MateriaPosterior { get => _materiaPosterior; set => _materiaPosterior = value; }
        public string Carrera { get => _carrera; set => _carrera = value; }
    }
}
