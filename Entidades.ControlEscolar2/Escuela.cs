﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Escuela
    {
        private string _nombre;
        private string _rfc;
        private string _domicilio;
        private int _telefono;
        private string _email;
        private string _pagina;
        private string _director;
        private string _logo;
        private int _id;

        public string Nombre { get => _nombre; set => _nombre = value; }
        public string Rfc { get => _rfc; set => _rfc = value; }
        public string Domicilio { get => _domicilio; set => _domicilio = value; }
        public int Telefono { get => _telefono; set => _telefono = value; }
        public string Email { get => _email; set => _email = value; }
        public string Pagina { get => _pagina; set => _pagina = value; }
        public string Director { get => _director; set => _director = value; }
        public string Logo { get => _logo; set => _logo = value; }
        public int Id { get => _id; set => _id = value; }
    }
}
