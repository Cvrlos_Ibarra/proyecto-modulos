﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.ControlEscolar2
{
    public class Municipios
    {
        private int codigomunicipio;
        private string municipio;
        private string fkestado;

        public int Codigomunicipio { get => codigomunicipio; set => codigomunicipio = value; }
        public string Municipio { get => municipio; set => municipio = value; }
        public string Fkestado { get => fkestado; set => fkestado = value; }
    }
}
