﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Entidades.ControlEscolar2
{
    public class Profesor
    {
        private string _numerocontrol;
        private string _nombre;
        private string _app;
        private string _apm;
        private string _direccion;
        private string _ciudad;
        private string _estado;
        private int _cedula;
        private string _titulo;
        private string _fechan;
       

        public string Numerocontrol { get => _numerocontrol; set => _numerocontrol = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string App { get => _app; set => _app = value; }
        public string Apm { get => _apm; set => _apm = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public string Ciudad { get => _ciudad; set => _ciudad = value; }
        public string Estado { get => _estado; set => _estado = value; }
        public int Cedula { get => _cedula; set => _cedula = value; }
        public string Titulo { get => _titulo; set => _titulo = value; }
        public string Fechan { get => _fechan; set => _fechan = value; }
        
    }
}
