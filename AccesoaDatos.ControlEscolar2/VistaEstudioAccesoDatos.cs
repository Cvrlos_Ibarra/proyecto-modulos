﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class VistaEstudioAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public VistaEstudioAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public List<VistaEstudio> GetEstudios(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listvistae = new List<VistaEstudio>();
            var ds = new DataSet();
            string consulta = "Select * from v_estudios2 where nombre like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "v_estudios2");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var vestudios = new VistaEstudio
                {

                    Clave = Convert.ToInt32(row["clave"]),
                    Ncontrol = row["n.control"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    Apellidop = row["apellido p"].ToString(),
                    Apellidom = row["apellido m"].ToString(),
                    Cedula = Convert.ToInt32(row["cedula"]),
                    Titulo = row["titulo"].ToString(),
                    Tituloextra = row["titulo extra"].ToString(),
                    Documento = row["documento"].ToString()




                };
                listvistae.Add(vestudios);
            }
            return listvistae;
        }
    }
}
