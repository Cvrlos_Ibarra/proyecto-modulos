﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class MateriasAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public MateriasAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Materias materia, string x)
        {

            if (x == "0")
            {
                //insertar
                //string consulta = "Insert into usuarios values(null,'" + usuario.Nombre + "',)";
                string consulta = string.Format("Insert into materia values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}') ",
                   materia.Idmateria, materia.Nombremateria, materia.Teoria, materia.Practica, materia.Semestre, materia.MateriaAnterior, materia.MateriaSiguiente, materia.Carrera);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update materia set nombremateria = '{0}',horasteoria = '{1}',horaspractica = '{2}', semestre = '{3}',fkmateriaAnterior = '{4}',fkmateriaSiguiente = '{5}',carrera = '{6}' where idmateria = '{7}'",
                      materia.Nombremateria, materia.Teoria, materia.Practica, materia.Semestre, materia.MateriaAnterior, materia.MateriaSiguiente, materia.Carrera, materia.Idmateria);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Guardar2(Materias materia, string x)
        {

            if (x == "0")
            {
                //insertar
                //string consulta = "Insert into usuarios values(null,'" + usuario.Nombre + "',)";
                string consulta = string.Format("Insert into materia values('{0}','{1}','{2}','{3}','{4}',null,null,'{5}') ",
                   materia.Idmateria, materia.Nombremateria, materia.Teoria, materia.Practica, materia.Semestre, materia.Carrera);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update materia set nombremateria = '{0}',horasteoria = '{1}',horaspractica = '{2}', semestre = '{3}',fkmateriaAnterior = null,fkmateriaSiguiente = null,carrera = '{4}' where idmateria = '{5}'",
                      materia.Nombremateria, materia.Teoria, materia.Practica, materia.Semestre, materia.MateriaAnterior, materia.MateriaSiguiente, materia.Carrera, materia.Idmateria);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Guardar3(Materias materia, string x)
        {

            if (x == "0")
            {
                //insertar
                //string consulta = "Insert into usuarios values(null,'" + usuario.Nombre + "',)";
                string consulta = string.Format("Insert into materia values('{0}','{1}','{2}','{3}','{4}',null,'{5}','{6}') ",
                   materia.Idmateria, materia.Nombremateria, materia.Teoria, materia.Practica, materia.Semestre, materia.MateriaSiguiente, materia.Carrera);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update materia set nombremateria = '{0}',horasteoria = '{1}',horaspractica = '{2}', semestre = '{3}',fkmateriaAnterior = null,fkmateriaSiguiente = '{5}',carrera = '{6}' where idmateria = '{7}'",
                      materia.Nombremateria, materia.Teoria, materia.Practica, materia.Semestre, materia.MateriaAnterior, materia.MateriaSiguiente, materia.Carrera, materia.Idmateria);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Guardar4(Materias materia, string x)
        {

            if (x == "0")
            {
                //insertar
                //string consulta = "Insert into usuarios values(null,'" + usuario.Nombre + "',)";
                string consulta = string.Format("Insert into materia values('{0}','{1}','{2}','{3}','{4}','{5}',null,'{6}') ",
                   materia.Idmateria, materia.Nombremateria, materia.Teoria, materia.Practica, materia.Semestre, materia.MateriaAnterior, materia.Carrera);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update materia set nombremateria = '{0}',horasteoria = '{1}',horaspractica = '{2}', semestre = '{3}',fkmateriaAnterior = '{4}',fkmateriaSiguiente = null,carrera = '{5}' where idmateria = '{6}'",
                      materia.Nombremateria, materia.Teoria, materia.Practica, materia.Semestre, materia.MateriaAnterior, materia.Carrera, materia.Idmateria);
                conexion.EjecutarConsulta(consulta);
            }

        }

        public void Eliminar(string id)
        {
            //eliminar
            string consulta = string.Format("Delete from materia where idmateria = '{0}'", id);
            conexion.EjecutarConsulta(consulta);
        }

      

        public List<Materias> GetMaterias(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listMaterias = new List<Materias>();
            var ds = new DataSet();
            string consulta = "Select * from materia where nombremateria like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "materia");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var materia = new Materias
                {
                    Idmateria = row["idmateria"].ToString(),
                    Nombremateria = row["nombremateria"].ToString(),
                    Teoria = Convert.ToInt32(row["horasteoria"]),
                    Practica = Convert.ToInt32(row["horaspractica"]),
                    Semestre = row["semestre"].ToString(),
                    MateriaAnterior = row["fkmateriaAnterior"].ToString(),
                    MateriaSiguiente = row["fkmateriaSiguiente"].ToString(),
                    Carrera = row["carrera"].ToString()

                };
                listMaterias.Add(materia);
            }
            return listMaterias;
        }
        
    }
}
