﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class EstadoAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public EstadoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public List<Estados> GetEstados(string filtro)
        {
            var listEstados = new List<Estados>();
            var ds = new DataSet();
            string consulta = "Select * from estados";

            ds = conexion.ObtenerDatos(consulta, "estados");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var estados = new Estados
                {
                    Codigoestado = row["codigoestado"].ToString(),
                    Estado = row["estado"].ToString()
                };
                listEstados.Add(estados);
            }
            return listEstados;
        }       
    }
}
