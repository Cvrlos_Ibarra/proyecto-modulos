﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class MunicipioAccesoDatos
    {

        ConexionAccesoDatos conexion;

        public MunicipioAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public List<Municipios> GetMunicipios(string filtro)
        {
            var listMunicipios = new List<Municipios>();
            var ds = new DataSet();
            string consulta = "Select * from municipios where fkestado like '%"+ filtro +"%'";

            ds = conexion.ObtenerDatos(consulta, "municipios");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var municipios = new Municipios
                {
                    Codigomunicipio = Convert.ToInt32(row["idmunicipio"]),
                    Municipio = row["municipio"].ToString(),
                    Fkestado = row["fkestado"].ToString()
                };
                listMunicipios.Add(municipios);
            }
            return listMunicipios;
        }
    }
}
