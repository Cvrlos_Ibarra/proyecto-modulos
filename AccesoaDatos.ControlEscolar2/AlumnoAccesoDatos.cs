﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class AlumnoAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public AlumnoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Alumno alumno)
        {
            if (alumno.Numerocontrol == 0)
            {
                //insertar
                //string consulta = "Insert into usuarios values(null,'" + usuario.Nombre + "',)";
                string consulta = string.Format("Insert into alumno values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}') ", 
                    alumno.Nombre, alumno.Apellidopaterno, alumno.Apellidomaterno, alumno.Fechanacimiento,alumno.Domicilio,alumno.Email,
                    alumno.Sexo,alumno.Estado,alumno.Municipio);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update alumno set nombre = '{0}',apellidopaterno = '{1}',apellidomaterno = '{2}'," +
                    "fechanacimiento = '{3}',domicilio = '{4}',email = '{5}',sexo = '{6}',estado = '{7}'," +
                    "municipio = '{8}' where numerocontrol = {9}", alumno.Nombre, alumno.Apellidopaterno, alumno.Apellidomaterno, 
                    alumno.Fechanacimiento, alumno.Domicilio, alumno.Email, alumno.Sexo, alumno.Estado, alumno.Municipio, 
                    alumno.Numerocontrol);
                conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int numerocontrol)
        {
            //eliminar
            string consulta = string.Format("Delete from alumno where numerocontrol = '{0}'", numerocontrol);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Alumno> GetAlumnos(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listalumno = new List<Alumno>();
            var ds = new DataSet();
            string consulta = "Select * from alumno where nombre like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "alumno");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var alumno = new Alumno
                {
                    Numerocontrol = Convert.ToInt32(row["numerocontrol"]),
                    Nombre = row["nombre"].ToString(),
                    Apellidopaterno = row["apellidopaterno"].ToString(),
                    Apellidomaterno = row["apellidomaterno"].ToString(),
                    Fechanacimiento = row["fechanacimiento"].ToString(),
                    Domicilio = row["domicilio"].ToString(),
                    Email = row["email"].ToString(),
                    Sexo = row["sexo"].ToString(),
                    Estado = row["estado"].ToString(),
                    Municipio = row["municipio"].ToString()

                };
                listalumno.Add(alumno);
            }
            return listalumno;
        }
    }
}
