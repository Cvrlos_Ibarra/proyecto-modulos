﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class ProfesorAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public ProfesorAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Profesor profesor,string x)
        {
            
            if (x == "0")
            {
                //insertar
                //string consulta = "Insert into usuarios values(null,'" + usuario.Nombre + "',)";
                string consulta = string.Format("Insert into profesor values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}') ",
                   profesor.Numerocontrol, profesor.Nombre, profesor.App, profesor.Apm, profesor.Direccion, profesor.Ciudad, profesor.Estado, profesor.Cedula, profesor.Titulo, profesor.Fechan);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update profesor set nombre = '{0}',apellidopaterno = '{1}',apellidomaterno = '{2}', direccion = '{3}',ciudad = '{4}',estado = '{5}',cedula = '{6}',titulo = '{7}', fechanacimiento = '{8}' where Numerocontrol = '{9}'",
                     profesor.Nombre, profesor.App, profesor.Apm, profesor.Direccion, profesor.Ciudad, profesor.Estado, profesor.Cedula, profesor.Titulo, profesor.Fechan, profesor.Numerocontrol);
                conexion.EjecutarConsulta(consulta);
            }

        }
        
        public void Eliminar(string numerocontrol)
        {
            //eliminar
            string consulta = string.Format("Delete from profesor where numerocontrol = '{0}'", numerocontrol);
            conexion.EjecutarConsulta(consulta);
        }

        public string GetIds(string filtro)
        {
            var ds = new DataSet();
            string consulta = string.Format("Select numerocontrol from profesor where numerocontrol like '%" + filtro + "%'");
            //ds = conexion.ObtenerDatos(consulta, "profesor");
            conexion.EjecutarConsulta(consulta);
           

            return consulta;
        }
        

        public List<Profesor> GetProfesores(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listprofesores = new List<Profesor>();
            var ds = new DataSet();
            string consulta = "Select * from profesor where nombre like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "profesor");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var profesor = new Profesor
                {
                    Numerocontrol = row["numerocontrol"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    App = row["apellidopaterno"].ToString(),
                    Apm = row["apellidomaterno"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Ciudad = row["ciudad"].ToString(),
                    Estado = row["estado"].ToString(),
                    Cedula = Convert.ToInt32(row["cedula"]),
                    Titulo = row["titulo"].ToString(),
                    Fechan = row["fechanacimiento"].ToString()
                   
                };
                listprofesores.Add(profesor);
            }
            return listprofesores;
        }
        /*public List<Profesor> GetId(string filtro)
        {
            var listprofesores = new List<Profesor>();
            var ds = new DataSet();
            string consulta = "select max(substring(numerocontrol,6)) from profesor where numerocontrol like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "profesor");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var profesor = new Profesor
                {
                    Numerocontrol = row["numerocontrol"].ToString(),
                    Nombre = row["nombre"].ToString(),
                    App = row["apellidopaterno"].ToString(),
                    Apm = row["apellidomaterno"].ToString(),
                    Direccion = row["direccion"].ToString(),
                    Ciudad = row["ciudad"].ToString(),
                    Estado = row["estado"].ToString(),
                    Cedula = Convert.ToInt32(row["cedula"]),
                    Titulo = row["titulo"].ToString(),
                    Fechan = row["fechanacimiento"].ToString()

                };
                listprofesores.Add(profesor);
            }
            return listprofesores;
        }*/
    }
}
