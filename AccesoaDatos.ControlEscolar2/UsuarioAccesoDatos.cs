﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class UsuarioAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public UsuarioAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Usuario usuario)
        {
            if (usuario.Idusuario == 0)
            {
                //insertar
                //string consulta = "Insert into usuarios values(null,'" + usuario.Nombre + "',)";
                string consulta = string.Format("Insert into usuario values(null,'{0}','{1}','{2}','{3}') ",usuario.Nombre,usuario.App,usuario.Apm,usuario.Contrasenia);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                //update
                string consulta = string.Format("update usuario set nombre = '{0}',apellidopaterno = '{1}',apellidomaterno = '{2}',contrasenia = '{3}' where idusuario = {4}", usuario.Nombre, usuario.App, usuario.Apm, usuario.Contrasenia, usuario.Idusuario);
                    conexion.EjecutarConsulta(consulta);
            }

        }
        public void Eliminar(int idusuario)
        {
            //eliminar
            string consulta = string.Format("Delete from usuario where idusuario = '{0}'", idusuario);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Usuario> GetUsuarios(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listUsuario = new List<Usuario>();
            var ds = new DataSet();
            string consulta = "Select * from usuario where nombre like '%"+filtro+"%'";

            ds = conexion.ObtenerDatos(consulta, "usuario");

            var dt = new DataTable();
            dt = ds.Tables[0];


            foreach (DataRow row in dt.Rows)
            {
                var usuario = new Usuario
                {
                    Idusuario = Convert.ToInt32(row["idusuario"]),
                    Nombre = row["nombre"].ToString(),
                    App = row["apellidopaterno"].ToString(),
                    Apm = row["apellidomaterno"].ToString(),
                    Contrasenia = row["contrasenia"].ToString()

                };
                listUsuario.Add(usuario);
            }



            //HardCodear
            listUsuario.Add(new Usuario
            {
                Idusuario = 1,
                Nombre = "Nestor",
                App = "Chico",
                Apm = "Rojas"
            }) ;


            //Llenar lista
            return listUsuario;
        }
    }
}


