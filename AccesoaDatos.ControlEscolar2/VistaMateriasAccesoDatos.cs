﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Entidades.ControlEscolar2;

namespace AccesoaDatos.ControlEscolar2
{
    public class VistaMateriasAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public VistaMateriasAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public List<VistaMaterias> GetMaterias(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listvistamateria = new List<VistaMaterias>();
            var ds = new DataSet();
            string consulta = "Select * from v_mat where nombremateria like '%" + filtro + "%'";

            ds = conexion.ObtenerDatos(consulta, "v_mat");

            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var materia = new VistaMaterias
                {

                    
                    Idmateria = row["idmateria"].ToString(),
                    Nombremateria = row["nombremateria"].ToString(),
                    Horasteoria = Convert.ToInt32(row["Horas teoria"]),
                    Horaspractica = Convert.ToInt32(row["Horas practica"]),
                    Creditos = Convert.ToInt32(row["creditos"]),
                    Semestre = row["semestre"].ToString(),
                    MateriaAnterior = row["Materia Anterior"].ToString(),
                    MateriaPosterior = row["Materia Posterior"].ToString(),
                    Carrera = row["carrera"].ToString()




                };
                listvistamateria.Add(materia);
            }
            return listvistamateria;
        }

    }
}
