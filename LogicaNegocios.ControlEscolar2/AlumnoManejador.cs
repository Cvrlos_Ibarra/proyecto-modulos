﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class AlumnoManejador
    {
        private AlumnoAccesoDatos _alumnoAccesoDatos = new AlumnoAccesoDatos();
        public void Guardar(Alumno alumno)
        {
            _alumnoAccesoDatos.Guardar(alumno);

        }
        public void Eliminar(int numerocontrol)
        {
            //eliminar
            _alumnoAccesoDatos.Eliminar(numerocontrol);
        }
        public List<Alumno> GetAlumnos(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listAlumno = _alumnoAccesoDatos.GetAlumnos(filtro);
            //Llenar lista
            return listAlumno;
        }
    }
}
