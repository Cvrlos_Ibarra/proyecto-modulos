﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;
using System.Data;

namespace LogicaNegocios.ControlEscolar2
{
    public class EscuelaManejador
    {
        private EscuelaAccesoDatos _escuelaAccesoDatos = new EscuelaAccesoDatos();
        public void Guardar(Escuela escuela)
        {
            _escuelaAccesoDatos.Guardar(escuela);

        }
        public void Eliminar(int id)
        {
            //eliminar
            _escuelaAccesoDatos.Eliminar(id);
        }
        public List<Escuela> GetEscuela(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listEscuela = _escuelaAccesoDatos.GetEscuela(filtro);
            //Llenar lista
            return listEscuela;
        }
        public DataSet consulta()
        {
            DataSet dt = _escuelaAccesoDatos.Consulta();

            return dt;
        }

    }
}
