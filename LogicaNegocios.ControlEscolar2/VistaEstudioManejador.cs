﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class VistaEstudioManejador
    {
        private VistaEstudioAccesoDatos _vistaEstudioAccesoDatos = new VistaEstudioAccesoDatos();

        public List<VistaEstudio> GetEstudios(string filtro)
        {

            var listVistas = _vistaEstudioAccesoDatos.GetEstudios(filtro);

            return listVistas;
        }
    }
}
