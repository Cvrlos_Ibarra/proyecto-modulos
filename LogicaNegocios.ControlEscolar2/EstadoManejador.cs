﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class EstadoManejador
    {
        private EstadoAccesoDatos _estadoAccesoDatos = new EstadoAccesoDatos();

        public List<Estados> GetEstados(string filtro)
        {
            var listEstados = _estadoAccesoDatos.GetEstados(filtro);

            return listEstados;
        }

    }
}
