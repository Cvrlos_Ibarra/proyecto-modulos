﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoaDatos.ControlEscolar2;
using Entidades.ControlEscolar2;

namespace LogicaNegocios.ControlEscolar2
{
    public class VistaMateriasManejador
    {
        private VistaMateriasAccesoDatos _vistaMateriasManejador = new VistaMateriasAccesoDatos();

        public List<VistaMaterias> GetMaterias(string filtro)
        {

            var listVistaMateria = _vistaMateriasManejador.GetMaterias(filtro);

            return listVistaMateria;
        }
    }
}
